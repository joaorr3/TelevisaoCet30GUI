﻿namespace TelevisaoCet30GUI
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.ButtonOnOff = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.LabelStatus = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// ButtonOnOff
			// 
			this.ButtonOnOff.BackColor = System.Drawing.Color.Gray;
			this.ButtonOnOff.FlatAppearance.BorderSize = 5;
			this.ButtonOnOff.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.ButtonOnOff.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ButtonOnOff.ForeColor = System.Drawing.Color.White;
			this.ButtonOnOff.Location = new System.Drawing.Point(339, 258);
			this.ButtonOnOff.Name = "ButtonOnOff";
			this.ButtonOnOff.Size = new System.Drawing.Size(95, 95);
			this.ButtonOnOff.TabIndex = 0;
			this.ButtonOnOff.Text = "ON";
			this.ButtonOnOff.UseVisualStyleBackColor = false;
			this.ButtonOnOff.Click += new System.EventHandler(this.ButtonOnOff_Click);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(100, 23);
			this.label1.TabIndex = 0;
			// 
			// LabelStatus
			// 
			this.LabelStatus.AutoSize = true;
			this.LabelStatus.Location = new System.Drawing.Point(12, 315);
			this.LabelStatus.Name = "LabelStatus";
			this.LabelStatus.Size = new System.Drawing.Size(69, 13);
			this.LabelStatus.TabIndex = 1;
			this.LabelStatus.Text = "Status da TV";
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
			this.ClientSize = new System.Drawing.Size(446, 365);
			this.Controls.Add(this.LabelStatus);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.ButtonOnOff);
			this.ForeColor = System.Drawing.Color.White;
			this.Name = "Form1";
			this.Opacity = 0.8D;
			this.ShowIcon = false;
			this.Text = "Form1";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button ButtonOnOff;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label LabelStatus;
	}
}

