﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TelevisaoCet30GUI
{
	public partial class Form1 : Form
	{
		private Tv minhaTV;

		public Form1()
		{
			InitializeComponent();
			minhaTV = new Tv();
			LabelStatus.Text = minhaTV.EnviaMensagem();
		}

		private void ButtonOnOff_Click(object sender, EventArgs e)
		{

			if (!minhaTV.GetEstado())
			{
				minhaTV.LigaTv();
				LabelStatus.Text = minhaTV.EnviaMensagem();
				ButtonOnOff.Text = "OFF";
				//ButtonAumentaCanal.Enabled = true;
				//ButtonDiminuiCanal.Enabled = true;
				//LabelCanal.Text = minhaTV.GetCanal().ToString();
			}
			else
			{
				minhaTV.DesligaTv();
				LabelStatus.Text = minhaTV.EnviaMensagem();
				ButtonOnOff.Text = "ON";
				//ButtonAumentaCanal.Enabled = false;
				//ButtonDiminuiCanal.Enabled = false;
				//LabelCanal.Text = "- -";
			}

		}

		private void Form1_Load(object sender, EventArgs e)
		{

		}

		


	}
}
